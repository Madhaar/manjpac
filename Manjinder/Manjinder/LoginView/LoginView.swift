//
//  ContentView.swift
//  Manjinder
//
//  Created by Singh, Manjinder on 29/07/2021.
//

import SwiftUI
import Combine

struct LoginView: View {
    
    @EnvironmentObject var settings: UserSettings
    var loginVM = LoginViewModel()
    
    var body: some View {
        
        Color.black
            .ignoresSafeArea()
            .overlay(
                VStack {
                    Logo()
                    TitleText()
                    Button(action: {
                        loginVM.login(withSettings: settings)
                        
                    }) {
                        Text("Login")
                        .frame(minWidth: 100, maxWidth: 300, minHeight: 44, maxHeight: 44, alignment: .center)
                    }
                    .background(Color.white)
                    .foregroundColor(Color.red)
                    .cornerRadius(10)
                    .alert(isPresented: $settings.showAlertForFailure, content: {
                        Alert(title: Text("Login"), message: Text("Login Unsuccessful"), dismissButton: .default(Text("OK")))
                    })
                }
            )
    }
    
}

struct Logo: View {
    var body: some View {
        Image("Logo")
            .resizable()
            .frame(width: 100, height: 90, alignment: .center)
    }
}

struct TitleText: View {
    var body: some View {
        Text("Hello World!")
            .foregroundColor(.white)
            .font(.largeTitle)
            .fontWeight(.bold)
            .padding(.bottom, 30)
    }
}
