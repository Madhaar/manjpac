//
//  HomeViewModel.swift
//  Manjinder
//
//  Created by Singh, Manjinder on 03/08/2021.
//

import Foundation

class HomeViewModel {
    
    func signout(withSettings: UserSettings) {
        SessionManager.shared.signOut { (result) in
            
            switch result {
            case .success(_):
                withSettings.loggedIn = false
            case .failure(_):
                withSettings.loggedIn = true
             withSettings.showAlertForFailure = true
            // if more time is given i would like to pass this error message to show in the alert.
            }
        }
    }
}
