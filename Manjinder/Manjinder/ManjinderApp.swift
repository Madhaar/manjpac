//
//  ManjinderApp.swift
//  Manjinder
//
//  Created by Singh, Manjinder on 29/07/2021.
//

import SwiftUI

@main
struct ManjinderApp: App {
    let userSettings = UserSettings()
    var body: some Scene {
        WindowGroup {
            StartView().environmentObject(userSettings)
        }
    }
}
