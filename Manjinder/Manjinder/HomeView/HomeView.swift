//
//  HomeView.swift
//  Manjinder
//
//  Created by Singh, Manjinder on 31/07/2021.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject var settings: UserSettings
     var homeViewModel = HomeViewModel()
    
    var body: some View {
        Color.gray
            .ignoresSafeArea()
            .overlay(
                VStack {
                    Text(SessionManager.shared.profile?.name ?? "Welcome")
                        .fontWeight(.bold)
                        .padding(EdgeInsets(top: 0, leading: 30, bottom: 70, trailing: 30))
                        .font(.largeTitle)
                        .minimumScaleFactor(0.01)
                    Button(action: {
                        homeViewModel.signout(withSettings: settings)
                        
                    }) {
                        Text("Logout")
                            .frame(minWidth: 100, maxWidth: 300, minHeight: 44, maxHeight: 44, alignment: .center)
                    }
                    .background(Color.black)
                    .foregroundColor(Color.red)
                    .cornerRadius(10)
                    .alert(isPresented: $settings.showAlertForFailure, content: {
                        Alert(title: Text("Logout"), message: Text("Logout Unsuccessful"), dismissButton: .default(Text("OK")))
                    })
                    
                }
            )
    }
}
