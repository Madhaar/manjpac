//
//  StartView.swift
//  Manjinder
//
//  Created by Singh, Manjinder on 02/08/2021.
//

import SwiftUI
import Auth0
import SimpleKeychain

final class UserSettings: ObservableObject {
    
    @Published var loggedIn : Bool = false
    @Published var showAlertForFailure: Bool = false
    
    init() {
        // if more time was allowed, i would like to show a loading spinner meanwhile we get this response back.
        SessionManager.shared.checkToken { (sucess) in
            self.loggedIn = sucess
        }
    }
}

struct StartView: View {
    
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        
        if settings.loggedIn {
            AnyView(HomeView().environmentObject(settings))
        } else {
            AnyView(LoginView().environmentObject(settings))
        }
    }
}
