//
//  LoginViewModel.swift
//  Manjinder
//
//  Created by Singh, Manjinder on 03/08/2021.
//

import SwiftUI
import Combine

struct LoginViewModel {
    
    func login(withSettings: UserSettings) {
        SessionManager.shared.signIn { (result) in
            switch result {
            case .success( _):
                withSettings.loggedIn = true
            case .failure( _):
                withSettings.loggedIn = false
             withSettings.showAlertForFailure = true
            // if more time is given i would like to pass this error message to show in the alert.
            }
        }
    }
}
