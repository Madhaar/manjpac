//
//  SignupManager.swift
//  Manjinder
//
//  Created by Singh, Manjinder on 31/07/2021.
//

import Auth0

protocol SessionManagerErrorProtocol: Error {
    var title: String? { get }
    var message: String { get }
}

enum SessionManagerError: SessionManagerErrorProtocol {
    case noConnection
    case invalidResponse
    case url(_ error: Error?)
    case decode(_ error: Error)
    case failed(reason: String)
    case dataLoad(statusCode: Int, data: Data)

    var message: String {
        switch self {
        case .noConnection:
            return "No internet connect"
        // Add messages for other cases
        default: return "Please try again later."
        }
    }

    var title: String? {
        return "Something went wrong"
    }
}

class SessionManager {
    
    static let shared = SessionManager()
    private let authentication = Auth0.authentication()
    var credentialsManager: CredentialsManager!
    var profile: UserInfo?
    var credentials: Credentials?
    
    private init () {
        self.credentialsManager = CredentialsManager(authentication: Auth0.authentication())
        self.credentialsManager.enableBiometrics(withTitle: "Touch to Authenticate")
         _ = self.authentication.logging(enabled: true) // API Logging
    }
    
    func signIn(completionHandler: @escaping (Result<Any>) -> Void) {
        guard let clientInfo = plistValues(bundle: Bundle.main) else {
            completionHandler(.failure(SessionManagerError.failed(reason: "Wrong set up")))
            return }
        Auth0
            .webAuth()
            .scope("openid profile")
            .audience("https://" + clientInfo.domain + "/userinfo")
            .start { result in
                switch result {
                case .failure(let error):
                    completionHandler(.failure(SessionManagerError.failed(reason: error.localizedDescription)))
                case .success(let credentials):
                    print("Credentials: \(credentials)")
                    _ = self.credentialsManager.store(credentials: credentials)
                    completionHandler(.success(true))
                }
            }
    }
    
    func signOut(completionHandler: @escaping (Result<Any>) -> Void) {
        
        Auth0
            .webAuth()
            .clearSession(federated: false) { result in
                self.credentialsManager.revoke { error in
                    guard error == nil else {
                        // Handle error
                        completionHandler(.failure(SessionManagerError.invalidResponse))
                        return
                    }
                    completionHandler(.success(true))
                }
            }
    }
    
    func checkToken(callback: @escaping (Bool) -> Void) {
        self.renewAuth { error in
            DispatchQueue.main.async {
                    guard error == nil else {
                        print("Failed to retrieve credentials: \(String(describing: error))")
                        return callback(false)
                    }
                    SessionManager.shared.retrieveProfile { error in
                        DispatchQueue.main.async {
                            guard error == nil else {
                                print("Failed to retrieve profile: \(String(describing: error))")
                                return callback(false)
                            }
                            callback(true)
                        }
                    }
            }
        }
    }
    
   fileprivate func retrieveProfile(_ callback: @escaping (Error?) -> ()) {
        guard let accessToken = self.credentials?.accessToken
            else { return callback(CredentialsManagerError.noCredentials) }
        self.authentication
            .userInfo(withAccessToken: accessToken)
            .start { result in
                switch(result) {
                case .success(let profile):
                    self.profile = profile
                    callback(nil)
                case .failure(let error):
                    callback(error)
                }
        }
    }
    
   fileprivate func renewAuth(_ callback: @escaping (Error?) -> ()) {
        guard self.credentialsManager.hasValid() else {
            return callback(CredentialsManagerError.noCredentials)
        }
        self.credentialsManager.credentials { error, credentials in
            guard error == nil, let credentials = credentials else {
                return callback(error)
            }
            self.credentials = credentials
            callback(nil)
        }
    }
    
    fileprivate  func plistValues(bundle: Bundle) -> (clientId: String, domain: String)? {
        guard
            let path = bundle.path(forResource: "Auth0", ofType: "plist"),
            let values = NSDictionary(contentsOfFile: path) as? [String: Any]
        else {
            print("Missing Auth0.plist file with 'ClientId' and 'Domain' entries in main bundle!")
            return nil
        }
        
        guard
            let clientId = values["ClientId"] as? String,
            let domain = values["Domain"] as? String
        else {
            print("Auth0.plist file at \(path) is missing 'ClientId' and/or 'Domain' entries!")
            print("File currently has the following entries: \(values)")
            return nil
        }
        return (clientId: clientId, domain: domain)
    }
}
